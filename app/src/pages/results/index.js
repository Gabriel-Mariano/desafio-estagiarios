import React, { useContext } from 'react';
import './style.css';

import Header from '../../components/header';
import Footer from '../../components/footer';
import Modal from '../../components/modal';
import {MyContext} from '../../state/context';

import userLogo from '../../assets/user-logo.png';

import {api} from '../../services/api';


function Results(){
    const {isModal,setIsModal, data, setData, setUserCurrent} = useContext(MyContext);


    async function handleSearch(props){
        const response = await api.get(`${props}`)
        setData([...data,response.data])
    }


 

    return(
        <>
        <Header onclick={handleSearch} />
        <div className="container">
            <div className="content" > 
                <section className="content-title" >
                    <span>Resultados para: Paytime</span>
                </section>
                <section className="content-card" style={data.length <1 ? { height:'100vh'} : {height:''}}>
                        {data.map((value,index)=>{
                            return(
                                <div key={index}> 
                                    <img src={value.avatar_url === null ? userLogo : value.avatar_url } alt="profile"/>
                                    <h2>{value.name === null ? "Nome não definido" : value.name}</h2>
                                    <a href={value.html_url} rel="noreferrer" target="_blank"> {value.html_url} </a>
                                    <span>Score: 1.00</span>

                                    <button type="button" 
                                            onClick={
                                                ()=>{
                                                    setUserCurrent([value])
                                                    setIsModal(true) 
                                                }
                                            }
                                    >
                                        VER MAIS
                                    </button>
                                </div>
                            );
                        })}
                </section>
            </div>
            {isModal? 
                <Modal />
                :
                <div></div>
            }
        </div>
        <Footer/>
        </>
    );
}

export default Results;