import React from 'react';
import './style.css';

import { Link } from 'react-router-dom';

function NotFount(){
    return(
        <>
        <h1>Página Não Encontrada!</h1>
       <Link to="/results">Ir para tela de busca.</Link>
        </>
    );
}

export default NotFount;