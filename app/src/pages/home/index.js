import React from 'react';
import './style.css';

import Header from '../../components/header';
import Footer from '../../components/footer';

import LogoShadow from '../../assets/LogoShadow.png';

function Home(){
    return(
        <>
        <Header/>
            <main>
                <img src={LogoShadow} alt="plano de fundo"/>
            </main>
        <Footer/>
        </>
    );
}

export default Home;