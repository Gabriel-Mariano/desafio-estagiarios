import React, {useState, createContext} from 'react';

export const MyContext = createContext();

export default function Context({children}){
    const [isModal,setIsModal] = useState(false);
    const [data,setData] = useState([]);
    const [userCurrent,setUserCurrent] = useState([]);

    return(
        <MyContext.Provider value={{isModal,setIsModal,data,setData,userCurrent,setUserCurrent}}>
            {children}
        </MyContext.Provider>
    );
}