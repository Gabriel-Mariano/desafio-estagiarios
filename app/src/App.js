import './App.css';
import Routes from './routes/routes';
import MyContext from './state/context';

function App() {
  return (
    <MyContext>
      <Routes/>
    </MyContext>
  );
}

export default App;
