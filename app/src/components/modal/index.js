import React, { useContext } from 'react';
import './style.css';

import userLogo from '../../assets/user-logo.png';
import {MyContext} from '../../state/context';


function Modal(){
    const {setIsModal, userCurrent} = useContext(MyContext);

    console.log(userCurrent)

    

    return(
        <>
        {userCurrent.map((value,i)=>{

            const dateBreak = value.created_at.split("T",2)
            const getDateBroken = dateBreak[0].split("-",3);
                  getDateBroken.reverse();
            const dateRegister = getDateBroken.join("-");

            
            return(
        // eslint-disable-next-line no-undef
        <div className="modal-blur" key={i}> 
            <div className="container-modal" >
            <section class="grid grid-template-areas-1">
                <div className="profile">
                    <img src={value.avatar_url === null ? userLogo : value.avatar_url } alt="profile"/>
                </div>
                <div className="title">
                    <div>
                        <h2>{value.name === null ? "Nome não definido" : value.name}</h2>
                    </div>
                </div>
                <div className="content-user">
                    <div>
                        <span><strong>Username:</strong></span>
                        <span> {value.name === null ? "Nome não definido" : value.name}</span>
                    </div>
                    <div>
                        <span><strong>Cadastrado(a):</strong></span>
                        <span>{dateRegister}</span>
                    </div>
                    <div>
                        <span><strong>URL:</strong></span>
                        <a href={value.html_url} rel="noreferrer" target="_blank"> {value.html_url} </a>
                    </div>
                </div>
                <div className="sidenav">
                    <div>
                        <span><strong>Seguindo:</strong></span>
                        <span>{value.following}</span>
                    </div>
                    <div>
                        <span><strong>Seguidores:</strong></span>
                        <span> {value.followers}</span>
                    </div>
                    <div>
                    <button type="button" onClick={()=>setIsModal(false)}>
                        FECHAR
                    </button>
                    </div>
                </div>
            </section>
            </div></div>
        
         ) })}
        </>
    );
}

export default Modal;