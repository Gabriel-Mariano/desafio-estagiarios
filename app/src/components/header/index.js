import React, {useState} from 'react';
import './style.css';
import {BsSearch} from 'react-icons/bs';


import Logo from '../../assets/Logo.png';


function Header(props){
    const [user,setUser] = useState(''); 

    return(
        <>
        <header>
            <img src={Logo} alt="logo"/>
            <div className="area-input">
                <input type="text" placeholder="Pesquisar" value={user} onChange={ (e)=>setUser(e.target.value) }/>
                <button onClick={()=>props.onclick(user)}>
                    <BsSearch size={21} color="#fff"/>
                </button> 
            </div>
        </header>
        </>
    );
}

export default Header;