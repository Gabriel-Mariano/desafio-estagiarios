import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from '../pages/home';
import Results from '../pages/results';
import NotFount from '../pages/notFound';


function Routes(){
    return(
        <Router>
            <Switch>
               {/* <Route exact path="/" component={Home}/>   */}
                <Route exact path="/" component={Results}/>
                <Route exact path="*" component={NotFount}/>
            </Switch>
        </Router>
    );
}

export default Routes;